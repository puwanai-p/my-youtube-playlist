import './App.css';
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Routes }from "react-router-dom";
import Videothumbnaillist from "./components/Videothumbnaillist";
import ReactYoutube from './components/ReactYoutube';

function App() {
    return (
        <Router>
            <Routes>
                <Route path={"/"} element={<Videothumbnaillist />} />
                <Route path={"/player/:videoId"} element={<ReactYoutube page={2} />} />
            </Routes>
        </Router>
  );
}

export default App;

import React from 'react';
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';

function NavBar() {
    return (
        <Navbar bg="dark" data-bs-theme="dark" fixed="top">
            <Container>
                <Navbar.Brand href="/">Home</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Item>
                            <Nav.Link target="_blank" href="https://gitlab.com/puwanai-p/my-youtube-playlist">Source Code</Nav.Link>
                        </Nav.Item>
                        {/* for further improve
                        <Nav.Item>
                            <Nav.Link href="/upload">Upload</Nav.Link>
                        </Nav.Item>
                        */}
                    </Nav>
                    <Nav className="ms-auto">
                        <Nav.Item>
                            <Nav.Link target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=puwanai.pongpitakchai@gmail.com">Contact me</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}


export default NavBar;
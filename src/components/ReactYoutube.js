import React from 'react';
import YouTube from 'react-youtube';
import { useParams } from 'react-router';
import NavBar from './NavBar';
import './ReactYoutube.css';
import './VideoPlayer.css';

const ReactYoutube = (props) => {

    //const videoId = 'Y-j4RXMXIUE'; // Replace with the actual YouTube video ID
    const { videoId } = useParams();
    const opts = {

        width: '100%',
        height: '100%',
        playerVars: {
            // You can customize player options here (e.g., autoplay, controls, etc.)
            autoplay: props.page == 2 ? 1 : 0, // Set to 1 for autoplay
            controls: props.page == 2 ? 1 : 0, // Show player controls
        },
    };
    return (
        props.page == 2 ?
            <div>
                <NavBar />
                <div className='wrapper-header'>
                    <YouTube style={{ padding: 10 }} className="react-player centered-video" videoId={props.videoId01 ? props.videoId01 : videoId} opts={opts} />
                </div>
            </div>
            :
            <div className="card col-lg-3 col-md-4 col-sm-6">
                <YouTube style={{ padding: 10 }} className="react-player" videoId={props.videoId01 ? props.videoId01 : videoId} opts={opts} />
                <div class="card-body">
                    <a href={"/player/" + (props.videoId01 ? props.videoId01 : videoId)} class="btn btn-primary">Play on individual page</a>
                </div>
            </div>
    );
};

export default ReactYoutube;
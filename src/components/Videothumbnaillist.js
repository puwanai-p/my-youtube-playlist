import React from "react";
import NavBar from "./NavBar";
import './VideoPlayer.css';
import ReactYoutube from "./ReactYoutube";

class Videothumbnaillist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            thumbnailList: [],
        }

    }


    async getThumbnailList(page) {
        
        //fetch('http://192.168.1.108:5715/api/getthumbnaillist')
        /*
        fetch('http://localhost:5715/api/getthumbnaillist')
            .then(async (response) => {
                let data = await response.json();
                console.log(data);
                this.setState({
                    thumbnailList: data
                })
            });
        */
        this.setState({
            thumbnailList: ["G6r1dAire0Y", "LtQUJMBH8uE", "TQ8WlA2GXbk", "nXO-wpoozu8"]
        })
    }

    componentDidMount() {
        this.getThumbnailList(this.state.page);
    }


    render() {
        const { thumbnailList } = this.state;
        return (
        <div>
                <NavBar/>
                <div className="wrapper-header d-flex justify-content-center" relative="true">
                    <div className="container-fluid text-center">
                        <div className="row">
                            {thumbnailList.map((v, k) => {
                                return <ReactYoutube videoId01={v} />
                            })
                        }
                        </div>
                    </div>
                </div>
        </div>
        );
    }
}

export default Videothumbnaillist;

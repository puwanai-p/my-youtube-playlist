const express = require('express');
const bodyParser = require('body-parser');
//const mongoose = require('mongoose');

const fs = require('fs');
const path = require('path');
const app = express();
const port = process.env.PORT || 5715;

const cors = require('cors'); // Import the cors middleware

// Middleware for parsing JSON requests
app.use(bodyParser.json());
app.use(cors());
// Connect to your database (e.g., MongoDB)
/*
mongoose.connect('mongodb://localhost:27017/mydatabase', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
*/
// Define a basic API route

app.get('/api/getthumbnaillist', (req, res) => {
    let directoryPath = '../src/thumbnails';
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            console.error('Error reading directory:', err);
            return;
        }
        const fileNamesArray = [];
        files.forEach((file) => {
            let fileNameWithoutExtension = path.basename(file, path.extname(file));
            fileNamesArray.push({ 'filename':fileNameWithoutExtension,'filefullname':file });
        });

        const jsonFileNames = JSON.stringify(fileNamesArray);

        res.json(jsonFileNames);
    });
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});